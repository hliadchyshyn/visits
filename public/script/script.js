const inputFields = {
    Dentist:[
        {
            code:"fullName",
            name:"ФИО"
        },
        {
            code:"visitPurpose",
            name:"Цель визита"
        },
        {
            code:"lastVisitDate",
            name:"Дата последнего посещения"
        },
        {
            code: "notes",
            name: "Комментарий"
        },
    ],

    Cardiologist:[
        {
            code:"fullName",
            name:"ФИО"
        },
        {
            code:"visitPurpose",
            name:"Цель визита"
        },
        {
            code:"pressure",
            name:"Обычное давление"
        },
        {
            code:"BMI",
            name:"Индекс массы тела"
        },
        {
            code:"cardiovascularDiseases",
            name:"Перенесенные заболевания сердечно-сосудистой системы"
        },
        {
            code:"age",
            name:"Возраст"
        },
        {
            code: "notes",
            name: "Комментарий"
        }
    ],

    Therapist:[
        {
            code:"fullName",
            name:"ФИО"
        },
        {
            code: "visitPurpose",
            name: "Цель визита"
        },
        {
            code: "age",
            name: "Возраст"
        },
        {
            code: "notes",
            name: "Комментарий"
        }
    ],
};

let visits = [];


class Visit {
    constructor(fullName, notes, visitPurpose){
        this.patientName = fullName;
        this.notes = notes;
        this.visitPurpose= visitPurpose;
    }
    getPatientName = ()=>{
        return this.patientName
    };
    getVisitNotes = ()=>{
        return this.notes
    };
    getVisitDate = ()=>{
        const now = new Date();
        this.visitDate = `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()}`;
        return this.visitDate;
    };
    getVisitName = ()=>{
        return `${this.visitDate} ${this.patientName}`
    };
    getVisitPurpose = ()=>{
        return this.visitPurpose
    };
}

class DentistVisit extends Visit{
    constructor (fullName, visitPurpose, lastVisitDate, notes){
        super(fullName,notes,visitPurpose);
        this.lastVisitDate = lastVisitDate;
        }
    getLastVisitDate = ()=>{
        return this.lastVisitDate
    };
}

class CardiologistVisit extends Visit{
    constructor (fullName, visitPurpose, pressure, BMI, cardiovascularDiseases, age, notes){
        super(fullName,notes, visitPurpose);
        this.pressure = pressure;
        this.BMI = BMI;
        this.cardiovascularDiseases = cardiovascularDiseases;
        this.age = age;
    }

    getPressure = ()=>{
        return this.pressure
    };
    getBMI= ()=>{
        return this.BMI
    };
    getCardiovascularDiseases = ()=>{
        return this.cardiovascularDiseases
    };
    getPatientAge = ()=>{
        return this.age
    };
}


class TherapistVisit extends Visit{
    constructor (fullName, visitPurpose, age, notes){
        super(fullName,notes, visitPurpose);
        this.age = age;
    }

    getPatientAge = ()=>{
        return this.age
    };
}


const modalWrapper = document.getElementById('open-modal');
const closeModal = document.getElementById('closeModal');
const openModal = document.getElementById('visitModal');

openModal.addEventListener('click', ()=>{
    modalWrapper.style.opacity = "1";
    modalWrapper.style.pointerEvents = "auto";
});
closeModal.addEventListener('click', (e) => {
        modalWrapper.style.opacity = '0';
        modalWrapper.style.pointerEvents = "none";
});
modalWrapper.addEventListener('click', (e) => {
    if (e.target === modalWrapper) {
        modalWrapper.style.opacity = '0';
        modalWrapper.style.pointerEvents = "none";
    }
});

const goToVisitDetails = document.getElementById('goToVisitDetails');

goToVisitDetails.addEventListener('click', ()=>{
    const doctorSelectValue = document.getElementById('doctorSelect').value;
    const visitFields = inputFields[doctorSelectValue];
    const frag = document.createDocumentFragment();
    for (let key of visitFields){
        let input = document.createElement('input');
        input.type = "text";
        input.className = "input input-visit-details";
        input.setAttribute('id', `${key.code}`);
        input.setAttribute('required', `true`);
        input.setAttribute('placeholder', `${key.name}`);
        frag.appendChild(input);
    };


    const form = document.getElementById('form');

    // document.getElementById('modalFooter').removeChild(document.getElementById('goToVisitDetails'));
    // form.removeChild(document.getElementById('doctorSelect'));
    const doctorSelect = document.getElementById('doctorSelect');
    doctorSelect.style.display = 'none';
    goToVisitDetails.style.display = 'none';


    const createVisitBtn = document.createElement("button");
    createVisitBtn.innerText = 'Create';
    createVisitBtn.setAttribute('class', `btn btn_details btn_sm`);
    createVisitBtn.id = 'createVisit';

    createVisitBtn.addEventListener('click', ()=>{
        // console.log('success');
        createVisitBtn.style.display = 'none';
        let formInputs = document.querySelectorAll('.input-visit-details');
        let visit;
        switch (doctorSelectValue) {
            case 'Dentist':
                visit = new DentistVisit(
                    document.getElementById('fullName').value,
                    document.getElementById('visitPurpose').value,
                    document.getElementById('lastVisitDate').value,
                    document.getElementById('notes').value);
                break;

            case 'Cardiologist':
                visit = new CardiologistVisit(
                    document.getElementById('fullName').value,
                    document.getElementById('visitPurpose').value,
                    document.getElementById('pressure').value,
                    document.getElementById('BMI').value,
                    document.getElementById('cardiovascularDiseases').value,
                    document.getElementById('age').value,
                    document.getElementById('notes').value);
                break;

            case 'Therapist':
                visit = new TherapistVisit(
                    document.getElementById('fullName').value,
                    document.getElementById('visitPurpose').value,
                    document.getElementById('age').value,
                    document.getElementById('notes').value);
                break;
        }
        visits.push(visit);
        localStorage.setItem('visits', JSON.stringify(visits));
        console.log(visit);
        console.log(visits);
        formInputs.forEach(element =>{
            element.remove();
        });
        document.getElementById('doctorSelect').style.display = 'block';
        goToVisitDetails.style.display = 'block';
        modalWrapper.style.opacity = '0';
        modalWrapper.style.pointerEvents = "none";


    });

    document.getElementById('modalFooter').appendChild(createVisitBtn);
    form.appendChild(frag);

});

// let visit = new DentistVisit('1233', 'schedule','2019-01-01');


